/* This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*/
/**Agnieszka Kołakowska**/
/**Dyspersja w pryzmacie**/
package pl.edu.pw.fizyka.pojava.dispersion_in_prism;


import java.awt.Color;
import javax.media.opengl.GL2;

public class ColorExtended {

	int wavelength = 0;
	
	float red =0;
    float green =0;
    float blue =0;
    
    public ColorExtended(){
    	wavelength = 0;
    	this.wavelengthToRGB();
    }
    
    public ColorExtended(int lambda){
    	wavelength = lambda;
    	this.wavelengthToRGB();
    }
    
    public ColorExtended(ColorExtended color){
    	this.wavelength = color.getWavelength(); 
    	this.red = color.getRed();
    	this.green = color.getGreen();
    	this.blue = color.getBlue();
    	
    }

	void wavelengthToRGB() {	//ze strony http://darekk.com/kalkulator/wavelength-to-rgb

	    if (wavelength >= 380 && wavelength <= 439) {
	        red =-((float)wavelength - 440) / (440 - 380);
	        green = 0.0f;
	        blue = 1.0f;
	    } 
	    else if (wavelength >= 440 && wavelength <= 489) {
	        red = 0.0f;
	        green =((float)wavelength - 440) / (490 - 440);
	        blue = 1.0f;
	    } 
	    else if (wavelength >= 490 && wavelength <= 509) {
	        red = 0.0f;
	        green = 1.0f;
	        blue =-((float)wavelength - 510) / (510 - 490);
	    } 
	    else if (wavelength >= 510 && wavelength <= 579) {
	        red = ((float)wavelength - 510) / (580 - 510);
	        green = 1.0f;
	        blue = 0.0f;
	    } 
	    else if (wavelength >= 580 && wavelength <= 644) {
	        red = 1.0f;
	        green = -((float)wavelength - 645) / (645 - 580);
	        blue = 0.0f;
	    } 
	    else if (wavelength >= 645 && wavelength <= 780) {
	        red = 1.0f;
	        green = 0.0f;
	        blue = 0.0f;
	    } 
	    else if (wavelength==1){ //bialy - do wizualizacji
	    	 red = 1.0f;
		     green = 1.0f;
		     blue = 1.0f;
	    }
	    else {		//czarny
	        red = 0.0f;
	        green = 0.0f;
	        blue = 0.0f;
	    }
	}
	
	public Color getColor(){
		return (new Color(red, green, blue));
		
	}
	public void setGLColor3f(GL2 gl){
		gl.glColor3f(red, green, blue);
		
	}
	
	public int getWavelength() {
		return wavelength;
	}

	public void setWavelength(int wavelength) {
		this.wavelength = wavelength;
	}

	public float getRed() {
		return red;
	}


	public float getGreen() {
		return green;
	}

	
	public float getBlue() {
		return blue;
	}
}