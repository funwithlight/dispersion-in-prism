/* This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*/
/**Agnieszka Kołakowska**/
/**Dyspersja w pryzmacie**/
package pl.edu.pw.fizyka.pojava.dispersion_in_prism;

import javax.vecmath.Vector2f;

public class CalculatorWhite {
	Visualisation visualisation;
    
    ColorExtended myColor = new ColorExtended();
    ColorExtended tempColor = new ColorExtended();
    
	//Dane poczatkowe
	float angle = 60;
    int length = 580;
    double n = 1.0d; //wsp zalamania
    float lightDimensionY = 0.0f;
    float lightDimensionX = 0.0f;
    String glass = "FS";
    
	public CalculatorWhite(Visualisation visualisation){
		this.visualisation = visualisation;
	}
	
	public void process (){
		visualisation.clear();

		//Wlasciwe obliczenia
		//TROJKAT
				VectorExtented2f vec1 = new VectorExtented2f(0.0f , -180.0f); 
				VectorExtented2f vec2= new VectorExtented2f(0.0f , 180.0f);
				
				VectorExtented2f vec9 = vec1.getRotatedWith(-(angle/2), vec2);
				VectorExtented2f vec7 = vec1.getRotatedWith(angle/2, vec2);
				
				float dif = vec9.getY() - vec1.getY();
				
				vec9.setY(vec9.getY()-dif);
				vec7.setY(vec7.getY()-dif);
				//Line l1 = new Line(v1.getVector(), v2.getVector()); //wysokosc
				Line l8 = new Line(vec9, vec2);//scianka
				Line l5 = new Line(vec2, vec7); //druga scianka trojkata
				Line l9 = new Line(vec9, vec7);//podstawa trojkata
				//Rysowanie trojkata
				visualisation.addLine(new Vertex(vec9, new ColorExtended(480)), new Vertex(vec2, new ColorExtended(480))); //scianka1
				visualisation.addLine(new Vertex(vec2, new ColorExtended(480)), new Vertex(vec7, new ColorExtended(480)));//scianka2
				visualisation.addLine(new Vertex(vec9, new ColorExtended(480)), new Vertex(vec7, new ColorExtended(480)));//podstawa trojkata
		
		//Promienie i rysowanie promieni
		for(length = 380; length<780; length+=1){
			myColor.setWavelength(length);
			n = calculateN(glass ,length);
			//Promienie i rysowanie promieni
			//Promien padajacy
			VectorExtented2f vec3 = new VectorExtented2f(-70*lightDimensionY , lightDimensionX*lightDimensionY);
			VectorExtented2f vec4 = new VectorExtented2f(lightDimensionY , lightDimensionY);
			Line l2 = new Line(vec3, vec4); //linia promienia1
			
			VectorExtented2f vec5 = intersectionPoint(l8, l2); //pierwszy punkt przeciecia
			//czy punkt przeciecia znajduje sie na rysowanej sciance
			if(vec5.getY()<vec2.getY() && vec5.getY()>vec9.getY()){
				visualisation.addLine(new Vertex(vec5, new ColorExtended(1)), new Vertex(l2.vectorM1000(), new ColorExtended(1)));//promien padajacy
			}
			else if(vec5.getY()>vec2.getY()){
				visualisation.addLine(new Vertex(l2.vector1000(), new ColorExtended(1)), new Vertex(l2.vectorM1000(), new ColorExtended(1)));//promien padajacy
				return;
			}
			else{
				visualisation.addLine(new Vertex(intersectionPoint(l9, l2), new ColorExtended(1)), new Vertex(l2.vectorM1000(), new ColorExtended(1)));//promien padajacy
				return;
			}
			Line l3 = l8.getNormal(vec5); //normalna
			
			//Kat padania
			float alpha = calculateIncidenceAngle(l2, l3); //alpha w stopniach
			
			Line l4 = refractionFirst(alpha, l3, vec5, n); 
			
			VectorExtented2f vec8 = intersectionPoint(l4, l5); //punkt przeciecia2
			Line ray;
			Line l6;
			//kat padania na 2 scianke
				float gamma;
			//czy punkt przeciecia znajduje sie na rysowanej sciance
					if(vec8.getY()<vec2.getY() && vec8.getY()>vec7.getY()){
						//visualisation.addLine(new Vertex(vec5, myColor), new Vertex(vec8, myColor)); //promien w srodku
						ray = l5;
						l6 = ray.getNormal(vec8); //normalna2
						//kat padania na 2 scianke
						Vector2f tmp1 = new VectorExtented2f(vec5.getX()-vec8.getX(), vec5.getY()-vec8.getY());
						VectorExtented2f tmp2 = new VectorExtented2f(l6.vectorM1000().getX()-vec8.getX(), l6.vectorM1000().getY()-vec8.getY());
						gamma = (float) (tmp1.angle(tmp2)*180/Math.PI);
						
					}
					else{
						ray = l9;
						vec8 = intersectionPoint(l4, ray); //punkt przeciecia2
						//VectorExtented2f normal = l9.getVectorNormal();
						
						l6 = new Line(vec8, new VectorExtented2f(vec8.getX(), 1000));
						
						Line l10 = new Line(vec5, vec8);
						gamma = (float) (Math.atan(l10.getA())*180/Math.PI); //stopnie
					}
			
			visualisation.addLine(new Vertex(vec5, myColor), new Vertex(vec8, myColor)); //promien w srodku
			
			
			
			//sprawdzanie calkowitego wewnetrznego odbicia
			Line l7;
			boolean lineSeven = true;
			VectorExtented2f odbite = null;
			if(Math.abs(gamma)<(Math.asin(1/n)*180/Math.PI)){
			//promien wychodzacy
				l7 = refractionSecond(gamma, l6, vec8, n);
				
			}
			else{
				//System.out.println(gamma +" wewnetrzne odbicie");
				
				VectorExtented2f odbitka = new VectorExtented2f(vec5.getX()-vec8.getX(), vec5.getY()-vec8.getY()).getRotated(2*gamma);
				odbite = new VectorExtented2f(vec8.getX() + odbitka.getX()*1000, vec8.getY() + odbitka.getY()*1000);
				l7 = new Line(vec8, odbite); //TU JEST BLAD cos jest nie tak ze skalowaniem punktu
				
				visualisation.addLine(new Vertex(odbite, myColor), new Vertex(vec8, myColor));
				lineSeven = false;
			}
			if (lineSeven) {
				visualisation.addLine(new Vertex(l7.vector1000(), myColor), new Vertex(vec8, myColor)); //promien wychodzacy
			} else {
				visualisation.addLine(new Vertex(vec8, myColor), new Vertex(odbite, myColor)); //promien wychodzacy
			}
			
			//Testy: normalne i punkty przeciecia

			//visualisation.addLine(new Vertex(l3.vector1000(), new ColorExtended(400)), new Vertex(l3.vectorM1000(), new ColorExtended(400)));//normalna1
			//visualisation.addVertex(new Vertex(vec5, new ColorExtended(500)));//punkt przeciecia

//			visualisation.addVertex(new Vertex(vec8, new ColorExtended(500)));//punkt przeciecia
//			visualisation.addLine(new Vertex(l6.vector1000(), new ColorExtended(400)), new Vertex(l6.vectorM1000(), new ColorExtended(400)));//normalna2
			
					
		}	
	}
	
	public String getGlass() {
		return glass;
	}

	public void setGlass(String glass) {
		this.glass = glass;
	}

	public void setAngle(float a){
		angle = a;
		
	}
	
	public void setLength(int l){
		length = l;
		
	}
	
	//Obliczanie punktu przeciecia
		public VectorExtented2f intersectionPoint(Line l1, Line l2){
			if(l1.getA()==l2.getA()){
				return new VectorExtented2f(0, 0);	
			}
			else{
				VectorExtented2f v = new VectorExtented2f();
				v.setX((l2.getB()-l1.getB())/(l1.getA()-l2.getA()));
				v.setY((v.getX()*(l1.getA()+l2.getA())+l1.getB()+l2.getB())/2);
				return v;
			}
		}
		
		//Obliczanie kata pomiedzy dwoma liniami
		public float calculateIncidenceAngle(Line l1, Line l2){	
			if(l1.getA()==l2.getA()){
				return -1;}

			else
				return (float)((Math.atan(l1.getA())-Math.atan(l2.getA()))*180/Math.PI);

		}
		
		//Funkcje obliczajace kat zalamania
		public Line refractionFirst(float alpha, Line normal, VectorExtented2f intersectionPoint, double n){
			float beta = (float)((Math.asin(Math.sin(Math.abs(alpha)*Math.PI/180)/n)));//Beta w radianach
			VectorExtented2f v= normal.getDircetion().getRotated(beta*180/(float)Math.PI);
			return new Line(v.getTranslated(intersectionPoint.getX(), intersectionPoint.getY()), intersectionPoint);
		}
		
		public Line refractionSecond(float alpha, Line normal, VectorExtented2f intersectionPoint, double n){
			float delta = (float)((Math.asin(Math.sin(Math.abs(alpha)*Math.PI/180)*n)));//delta w radianach
			VectorExtented2f v= normal.getDircetion().getRotated(-(delta*180/(float)Math.PI));
			return new Line(v.getTranslated(intersectionPoint.getX(), intersectionPoint.getY()), intersectionPoint);
		}

		//Obliczanie wspolczynnika zalamania
		public double calculateN(String glass, int length){
			double[] factorFs  = { 0.6961663, 0.00467914825849, 0.4079426, 0.01351206307396, 0.8974794, 97.93400253792099};
			double[] factorAMTIR3 = {5.8505, 0.0852172864, 1.4536, 1824.485796, 0.0, 0.0};
			double[] factorLaFS  = { 2.00029547, 0.0121426017, 0.298926886, 0.0538736236, 1.80691843, 156.530829};
			double[] factorDiamond  = { 0.3306, 0.030625, 4.3356, 0.011236, 0.0, 0.0};
			double wavelength = (double)length/1000;		
			if(glass.equals("FS")){
				return Math.sqrt(1+
						((factorFs[0]*wavelength*wavelength)/((wavelength*wavelength)-factorFs[1]))+
						((factorFs[2]*wavelength*wavelength)/((wavelength*wavelength)-factorFs[3]))+
						((factorFs[4]*wavelength*wavelength)/((wavelength*wavelength)-factorFs[5])));
				
			}
			else if(glass.equals("AMTIR-3")){
				return Math.sqrt(1+
						(factorAMTIR3[0]*wavelength*wavelength/(wavelength*wavelength-factorAMTIR3[1]))+
						(factorAMTIR3[2]*wavelength*wavelength/(wavelength*wavelength-factorAMTIR3[3]))+
						(factorAMTIR3[4]*wavelength*wavelength/(wavelength*wavelength-factorAMTIR3[5])));
				
				
			}
			else if(glass.equals("LaSF")){
				return  Math.sqrt(1+
						(factorLaFS[0]*wavelength*wavelength/(wavelength*wavelength-factorLaFS[1]))+
						(factorLaFS[2]*wavelength*wavelength/(wavelength*wavelength-factorLaFS[3]))+
						(factorLaFS[4]*wavelength*wavelength/(wavelength*wavelength-factorLaFS[5])));
				
				
			}
			else if(glass.equals("Diamond")){
				return  Math.sqrt(1+
						(factorDiamond[0]*wavelength*wavelength/(wavelength*wavelength-factorDiamond[1]))+
						(factorDiamond[2]*wavelength*wavelength/(wavelength*wavelength-factorDiamond[3]))+
						(factorDiamond[4]*wavelength*wavelength/(wavelength*wavelength-factorDiamond[5])));
				
				
			}
			else if(glass.equals("TlBr-TlCl")){
				return Math.sqrt(1+3.821*Math.pow(wavelength,2)/(Math.pow(wavelength,2)-Math.pow(0.02234,2))-0.000877*Math.pow(wavelength,2));
			}
			else
				return 1.0d;
			
			
		}
		
		//Funkcja do ustawienia polozenia zrodla swiatla
		public void setLightDimension(int settedX, int settedY){
			this.lightDimensionX = ((float)settedX);
			this.lightDimensionY = ((float)settedY);
			
		}
		
		
	}