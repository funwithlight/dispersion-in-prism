/* This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*/
/**Agnieszka Kołakowska**/
/**Dyspersja w pryzmacie**/
package pl.edu.pw.fizyka.pojava.dispersion_in_prism;

import javax.media.opengl.GL2;

public class Vertex {
	
	VectorExtented2f vector;
	ColorExtended color;
	
	public Vertex(){
		this.vector = new VectorExtented2f();
		this.color = new ColorExtended();
	}
	
	public Vertex(float x, float y, int lambda){
		this.vector = new VectorExtented2f(x, y);
		this.color = new ColorExtended(lambda);
	}
	
	public Vertex(float x, float y, ColorExtended color){
		this.vector = new VectorExtented2f();
		this.vector.setX(x);
		this.vector.setY(y);
		this.color = new ColorExtended(color.getWavelength());
	}
	
	public Vertex(VectorExtented2f vector, ColorExtended color){
		this.vector = new VectorExtented2f();
		this.vector.setX(vector.getX());
		this.vector.setY(vector.getY());
		this.color = new ColorExtended(color.getWavelength());
	}
	
	public Vertex(Vertex v){
		this.vector = new VectorExtented2f(v.getVector());
		this.color = new ColorExtended(v.getColor());
	}
	
	public float getX() {
		return this.vector.getX();
	}

	public void setX(float x) {
		this.vector.getX();
	}

	public float getY() {
		return this.vector.getY();
	}

	public void setY(float y) {
		this.vector.getY();
	}
	
	public VectorExtented2f getVector(){
		return this.vector;
	}

	public ColorExtended getColor() {
		return color;
	}

	public void setVector(VectorExtented2f vector) {
		this.vector = vector;
	}

	public void setColor(ColorExtended color) {
		this.color = color;
	}
	
	public void paintColor(GL2 gl) {
		this.color.setGLColor3f(gl);
	}

}
