/* This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*/
/**Agnieszka Kołakowska**/
/**Dyspersja w pryzmacie**/
package pl.edu.pw.fizyka.pojava.dispersion_in_prism;

class Line
{
	float a = 0;
	float b = 0;
	
	public Line(){
		this.a = 0;
		this.b = 0; 
	
	}
	
	public Line(VectorExtented2f v1,VectorExtented2f v2){
		if(v1.getX()== v2.getX()){
			this.a = 0;
			this.b = 0;
		}

		else {
			this.a = (v1.getY()-v2.getY())/(v1.getX()-v2.getX());
			this.b = (v1.getY()+v2.getY()-this.a*(v1.getX()+v2.getX()))/2;
		}
				
	}
	
	//Linia normalna (prostopadla)
	public Line getNormal(VectorExtented2f v){
		Line l;
		float b2;
		float atemp = this.a;
		
		if(atemp==0){
			l = new Line(v, new VectorExtented2f(0, 0));
		}
		else{
			b2=v.getY()+(v.getX()/atemp);
			l = new Line(new VectorExtented2f((1)*v.getX(), -(1/atemp)*(1)*v.getX()+b2), new VectorExtented2f((-1)*v.getX(), -(1/atemp)*(-1)*v.getX()+b2));
		}	
		return l;
				
	}
	

	public float getA() {
		return a;
	}

	public float getB() {
		return b;
	}
	
	//Do rysowania lini (w przyblizeniu nieskonczonej)
	public VectorExtented2f vector1000(){
		return new VectorExtented2f(1000, this.a*1000+this.b);
	}
	public VectorExtented2f vectorM1000(){
		return new VectorExtented2f(-1000, this.a*(-1000)+this.b);
	}
	
	//Wektor kierunkowy
	public VectorExtented2f getDircetion (){
		double alpha = Math.atan(this.a);//radiany
		//System.out.println("A: "+this.a+" Wektor kierunkowy: "+alpha*180/Math.PI);
		return new VectorExtented2f((float)Math.cos(alpha), (float)Math.sin(alpha));
	}
	
	public VectorExtented2f getVectorNormal(){
		return new VectorExtented2f(this.getDircetion().getRotated(90));
	}
	public boolean isLineVertical(VectorExtented2f v1, VectorExtented2f v2){
		if (v1.getX()==v2.getX())
			return true;
		else
			return false;
	}
}
