/* This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*/
/**Agnieszka Ko�akowska**/
/**Dyspersja w pryzmacie**/

package pl.edu.pw.fizyka.pojava.dispersion_in_prism;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.awt.GLCanvas;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import com.jogamp.opengl.util.FPSAnimator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Scanner; 


public class Window extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	JList<String> listGlass; //Lista szkielek
	private Visualisation visualisation;
	private CalculatorWhite calcWhite;
	private Calculator calc;
	private Diagrams diagrams;
	//Do zmiany j�zyka
	
	static URL pl = Window.class.getResource("pl.txt");
	static URL eng = Window.class.getResource("eng.txt");
	static File translatorPl = new File(pl.getPath());
	static File translatorEng = new File(eng.getPath());
	
	private static Scanner in;
   
	//Ustawione parametry
	final JSlider waveLength;
	//charty
	final ChartPanel chartPanel1; 
	final ChartPanel chartPanel2; 
	final ChartPanel chartPanel3;
	final ChartPanel chartPanel4;
	
	float settedApexAngle = (float) ((Math.PI)/3);
	int settedWaveLength = 580;
	String settedGlass = "FS";
	
	int settedLightDimensionX = 0;
	int settedLightDimensionY = 0;
		
	boolean isWhite = false;
	
	//KONSTRUKTOR
	public Window() throws HeadlessException, IOException {
		//Tytul okienka
		super(Text(0, translatorPl));
		
		//Wizualizacja
		visualisation = new Visualisation();
		calcWhite = new CalculatorWhite(visualisation);
		calc = new Calculator(visualisation);
		diagrams = new Diagrams(calc);
		
		//Labele wszytskie dla jezyka
		final JLabel glassLabel =new JLabel(Text(1, translatorPl));
		final JLabel chromeLabel =new JLabel(Text(2, translatorPl));
		final JLabel waveLengthLabel =new JLabel(Text(3, translatorPl));
		final JLabel apexAngleLabel =new JLabel(Text(4, translatorPl));
		final JLabel languageLabel =new JLabel(Text(5, translatorPl));
		final JLabel lightDimensionLabel =new JLabel(Text(8, translatorPl));
			
		
		//Zamykanie okienka
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		//Rozmiar i pozycja okienka
		setSize(1000,800);
		setLocation(100, 100);
		
		//Blokowanie rozmiaru okienka
		setResizable(false);
		
		//Layout okna
		setLayout(new BorderLayout());
			
		//dolny panel
		final JPanel bottomPanel = new JPanel();
		
		//Panel ustwien
		JPanel settings = new JPanel(new GridLayout(13, 1));
		settings.setBackground(Color.GRAY);
		settings.setPreferredSize(new java.awt.Dimension(200, 0));
		add (settings, BorderLayout.LINE_END);
		
		
				
		//Wybor szkla
		settings.add(glassLabel);
		
		DefaultListModel<String> modelGlass = new DefaultListModel<String>();//Elementy listy
		modelGlass.addElement("FS");
		modelGlass.addElement("AMTIR-3");
		modelGlass.addElement("LaSF");
		modelGlass.addElement("Diamond");
		modelGlass.addElement("TlBr-TlCl");
		
		listGlass = new JList<String>(modelGlass);//Lista
		JScrollPane scrollList = new JScrollPane();
		scrollList.setViewportView(listGlass);
		settings.add(scrollList);
		listGlass.setSelectedIndex(0);
		
		listGlass.addListSelectionListener(new ListSelectionListener()
	    {
		      public void valueChanged(ListSelectionEvent e)
		      {
		    	  settedGlass = listGlass.getSelectedValue();

		    	 if(!isWhite){
		    		 calc.setGlass(settedGlass);
		    		 calc.process();
		    		 diagrams.process(settedGlass, settedWaveLength, settedApexAngle, settedLightDimensionX, settedLightDimensionY);
		    	 }
		    	 else if(isWhite){
		    		 calcWhite.setGlass(settedGlass);
		    		 calcWhite.process();
		    	 } 
		    }
	    });
		
		//Rodzaj fali
		settings.add(chromeLabel);
				
		JPanel chromeButtons = new JPanel(new GridLayout(1, 2));
				
		final JButton setMono = new JButton(Text(6, translatorPl));
		setMono.setBackground(Color.WHITE);	
		final JButton setWhite = new JButton(Text(7, translatorPl));
		setWhite.setBackground(Color.WHITE);
				
		ActionListener setChooseMonoListner = new ActionListener() {
			 @SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e){
				 isWhite= false;
				 waveLength.setBackground(new ColorExtended(waveLength.getValue()).getColor());
				 waveLength.enable();
				 chartPanel1.enable();
				 chartPanel2.enable();
				 chartPanel3.enable();
				 chartPanel4.enable();
				 calc.setAngle(settedApexAngle);
				 calc.setLightDimension(settedLightDimensionX, settedLightDimensionY);
				 calc.process();
				 bottomPanel.setVisible(true);
				 diagrams.process(settedGlass, settedWaveLength, settedApexAngle, settedLightDimensionX, settedLightDimensionY);
			}
		};
		ActionListener setChooseWhiteListner = new ActionListener() {
			 @SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e){
				 isWhite= true;
				 waveLength.setBackground(Color.GRAY);
				 waveLength.disable();
				 chartPanel1.disable();
				 chartPanel2.disable();
				 chartPanel3.disable();
				 chartPanel4.disable();
				 bottomPanel.setVisible(false);
				 calcWhite.setAngle(settedApexAngle);
				 calcWhite.setLightDimension(settedLightDimensionX, settedLightDimensionY);
				 calcWhite.process();
			}
		};
				
		setMono.addActionListener(setChooseMonoListner);
		setWhite.addActionListener(setChooseWhiteListner);
		chromeButtons.add(setMono);
		chromeButtons.add(setWhite);
		settings.add(chromeButtons);
				
		//Dlugosc fali
		final ColorExtended myColor = new ColorExtended(0);
		
		settings.add(waveLengthLabel);
		
	 
		waveLength =   new JSlider(JSlider.HORIZONTAL, 380, 780, 580);

	    waveLength.setMajorTickSpacing(100);
	    waveLength.setMinorTickSpacing(50);
	    waveLength.setPaintTicks(true);
	    waveLength.setPaintLabels(true);
	    waveLength.setUI(new javax.swing.plaf.basic.BasicSliderUI(waveLength));
	    myColor.setWavelength(waveLength.getValue());
		myColor.wavelengthToRGB();
	    waveLength.setBackground(myColor.getColor());
	    waveLength.addChangeListener(new ChangeListener() {
			
			public void stateChanged(ChangeEvent arg0) {
				myColor.setWavelength(waveLength.getValue());
				myColor.wavelengthToRGB();
				waveLength.setBackground(myColor.getColor());
				
				settedWaveLength = waveLength.getValue();
				if(!isWhite){
					calc.setLength(settedWaveLength);
					calc.process();
					diagrams.process(settedGlass, settedWaveLength, settedApexAngle, settedLightDimensionX, settedLightDimensionY);
		    	 }
			}
		});

		settings.add(waveLength);
		
		//Kat lamiacy
		settings.add(apexAngleLabel);
		
	    final JSlider apexAngle =   new JSlider(JSlider.HORIZONTAL, 0, 90, 60);
	    apexAngle.setMajorTickSpacing(10);
	    //apexAngle.setMinorTickSpacing(16);
	    apexAngle.setPaintTicks(true);
	    apexAngle.setPaintLabels(true);
	    apexAngle.setUI(new javax.swing.plaf.basic.BasicSliderUI(apexAngle));
		apexAngle.addChangeListener(new ChangeListener() {
			
			public void stateChanged(ChangeEvent arg0) {
				if(apexAngle.getValue()==0)
					settedApexAngle = 1;
				else
					settedApexAngle = (float) (apexAngle.getValue()); //przekazywanie w stopniach
				if(!isWhite){
					calc.setAngle(settedApexAngle);
					calc.process();
					diagrams.process(settedGlass, settedWaveLength, settedApexAngle, settedLightDimensionX, settedLightDimensionY);
		    	 }
		    	 else if(isWhite){
		    		 calcWhite.setAngle(settedApexAngle);
		    		 calcWhite.process();
		    	 }
			}
		});
		settings.add(apexAngle);
		
		//Polozenie zrodla swiatla

		settings.add(lightDimensionLabel);
		final JSlider lightDimensionX =   new JSlider(JSlider.HORIZONTAL, -30, 30, 0);
		lightDimensionX.setMajorTickSpacing(10);
		lightDimensionX.setMinorTickSpacing(5);
		lightDimensionX.setPaintTicks(true);
		lightDimensionX.setPaintLabels(true);
		lightDimensionX.setUI(new javax.swing.plaf.basic.BasicSliderUI(lightDimensionX));
		
		final JSlider lightDimensionY =   new JSlider(JSlider.HORIZONTAL, -120, 120, 0);
		lightDimensionY.setMajorTickSpacing(40);
		lightDimensionY.setMinorTickSpacing(20);
		lightDimensionY.setPaintTicks(true);
		lightDimensionY.setPaintLabels(true);
		lightDimensionY.setUI(new javax.swing.plaf.basic.BasicSliderUI(lightDimensionY));
		
		ChangeListener lightDimensionListner = new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				settedLightDimensionX = lightDimensionX.getValue();
				settedLightDimensionY = lightDimensionY.getValue();
				if(!isWhite){
					calc.setLightDimension(settedLightDimensionX, settedLightDimensionY);
					calc.process();
					diagrams.process(settedGlass, settedWaveLength, settedApexAngle, settedLightDimensionX, settedLightDimensionY);
				}
				else if(isWhite){
					calcWhite.setLightDimension(settedLightDimensionX, settedLightDimensionY);
					calcWhite.process();
				}
			}
		};

		lightDimensionX.addChangeListener(lightDimensionListner);
		settings.add(lightDimensionX);
		lightDimensionY.addChangeListener(lightDimensionListner);
		settings.add(lightDimensionY);
		
		//Jezyk
		settings.add(languageLabel);
		
		JPanel languageButtons = new JPanel(new GridLayout(1, 2));
		
		JButton setLanguagePl = new JButton("PL");
		setLanguagePl.setBackground(Color.WHITE);	
		
		ActionListener setLanguagePlListner = new ActionListener() {
			 public void actionPerformed(ActionEvent e){
				 try {
					 	setTitle(Text(0, translatorPl));
						glassLabel.setText(Text(1, translatorPl));
						chromeLabel.setText(Text(2, translatorPl));
						waveLengthLabel.setText(Text(3, translatorPl));
						apexAngleLabel.setText(Text(4, translatorPl));
						languageLabel.setText(Text(5, translatorPl));
						setMono.setText(Text(6, translatorPl));
						setWhite.setText(Text(7, translatorPl));
						lightDimensionLabel.setText(Text(8, translatorPl));
				 } catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
				}
			}
		};
		setLanguagePl.addActionListener(setLanguagePlListner);
		languageButtons.add(setLanguagePl);
		
		JButton setLanguageEng = new JButton("ENG");
		setLanguageEng.setBackground(Color.WHITE);
		ImageIcon imgEng = new ImageIcon("engFlag.getPath()");
		setLanguageEng.setIcon(imgEng);
		ActionListener setLanguageEngListner = new ActionListener() {
			 public void actionPerformed(ActionEvent e){
				try {
					setTitle(Text(0, translatorEng));
					glassLabel.setText(Text(1, translatorEng));
					chromeLabel.setText(Text(2, translatorEng));
					waveLengthLabel.setText(Text(3, translatorEng));
					apexAngleLabel.setText(Text(4, translatorEng));
					languageLabel.setText(Text(5, translatorEng));
					setMono.setText(Text(6, translatorEng));
					setWhite.setText(Text(7, translatorEng));
					lightDimensionLabel.setText(Text(8, translatorEng));
				} catch (FileNotFoundException e1){ 
				// TODO Auto-generated catch block
					e1.printStackTrace();
			 	}
			 }
		};
		
		setLanguageEng.addActionListener(setLanguageEngListner);
		languageButtons.add(setLanguageEng);
		settings.add(languageButtons);
		
		// Inicjalizacja JOGL
    	GLProfile profile = GLProfile.get(GLProfile.GL2);
    	GLCapabilities capabilities = new GLCapabilities(profile);
    	// Tworzenie canvas
    	GLCanvas glcanvas = new GLCanvas(capabilities);
    	glcanvas.addGLEventListener(visualisation);
    	
    	this.settedApexAngle = (float) (apexAngle.getValue());
    	calc.setAngle(settedApexAngle);
		calc.setLength(settedWaveLength);
    	calc.process();
    	diagrams.process(settedGlass, settedWaveLength, settedApexAngle, settedLightDimensionX, settedLightDimensionY);
    	
    	FPSAnimator animator = new FPSAnimator(glcanvas, 60);
    	animator.start();
    	     	
    	this.getContentPane().add(glcanvas, BorderLayout.CENTER);
    	
    	//Wykresy
    	bottomPanel.setLayout(new GridLayout());
		bottomPanel.setPreferredSize(new java.awt.Dimension(0, 170));
		
		XYSeriesCollection collNLambda = new XYSeriesCollection(diagrams.getNLambda());
		XYSeries point1 = diagrams.getPointNLambda();
		collNLambda.addSeries(point1);
		
		XYSeriesCollection collDeltaLambda = new XYSeriesCollection(diagrams.getDeltaLambda());
		XYSeries point2 = diagrams.getPointDeltaLambda();
		collDeltaLambda.addSeries(point2);
		
		XYSeriesCollection collDeltaPhi = new XYSeriesCollection(diagrams.getDeltaPhi());
		XYSeries point3 = diagrams.getPointDeltaPhi();
		collDeltaPhi.addSeries(point3);
		
		XYSeriesCollection collDeltaN = new XYSeriesCollection(diagrams.getDeltaN());
		XYSeries point4 = diagrams.getPointDeltaN();
		collDeltaN.addSeries(point4);
				
		JFreeChart chartNLambda = ChartFactory.createXYLineChart 
				(null,  // Title
			     "lambda [nm]",           // X-Axis label
			     "n",           // Y-Axis label
			     collNLambda,          // Dataset
			     PlotOrientation.VERTICAL,        //Plot orientation
			     false,                //show legend
			     true,                // Show tooltips
			     false               //url show
			);
		
		final XYPlot plot1 = chartNLambda.getXYPlot();
		
		final ValueAxis rangeAxis1 = plot1.getRangeAxis();
        rangeAxis1.setUpperMargin(0.1);
        rangeAxis1.setLowerMargin(-0.65);
        
		final XYLineAndShapeRenderer renderer1 = new XYLineAndShapeRenderer();
        renderer1.setSeriesLinesVisible(1, false);
        renderer1.setSeriesShapesVisible(0, false);
        plot1.setRenderer(renderer1);
        
		JFreeChart chartDeltaLambda = ChartFactory.createXYLineChart 
			     (null,  // Title
			      "lambda [nm]",           // X-Axis label
			      "delta [deg]",           // Y-Axis label
			      collDeltaLambda,          // Dataset
			      PlotOrientation.VERTICAL,        //Plot orientation
			      false,                //show legend
			      true,                // Show tooltips
			      false               //url show
		);
	    final XYPlot plot2 = chartDeltaLambda.getXYPlot();
	       
	    final ValueAxis rangeAxis2 = plot2.getRangeAxis();
	    rangeAxis2.setUpperMargin(0.1);
	    //rangeAxis2.setLowerMargin(-0.5);
	        
	    final XYLineAndShapeRenderer renderer2 = new XYLineAndShapeRenderer();
	    renderer2.setSeriesLinesVisible(1, false);
	    renderer2.setSeriesShapesVisible(0, false);
	    plot2.setRenderer(renderer2);
	       
		JFreeChart chartDeltaPhi = ChartFactory.createXYLineChart 
			     (null,  // Title
			      "phi [deg]",           // X-Axis label
			      "delta [deg]",           // Y-Axis label
			      collDeltaPhi,          // Dataset
			      PlotOrientation.VERTICAL,        //Plot orientation
			      false,                //show legend
			      true,                // Show tooltips
			      false               //url show
		);
		
        final XYPlot plot3 = chartDeltaPhi.getXYPlot();
        
        final ValueAxis rangeAxis3 = plot3.getRangeAxis();
        rangeAxis3.setUpperMargin(0.1);
        rangeAxis3.setLowerMargin(-0.0);
        
		final XYLineAndShapeRenderer renderer3 = new XYLineAndShapeRenderer();
        renderer3.setSeriesLinesVisible(1, false);
        renderer3.setSeriesShapesVisible(0, false);
        plot3.setRenderer(renderer3);

		JFreeChart chartDeltaN = ChartFactory.createXYLineChart 
			     (null,  // Title
			      "n",           // X-Axis label
			      "delta [deg]",           // Y-Axis label
			      collDeltaN,          // Dataset
			      PlotOrientation.VERTICAL,        //Plot orientation
			      false,                //show legend
			      true,                // Show tooltips
			      false               //url show
		);
        final XYPlot plot4 = chartDeltaN.getXYPlot();
        
        final ValueAxis rangeAxis4 = plot4.getRangeAxis();
        rangeAxis4.setUpperMargin(0.05);
        rangeAxis4.setLowerMargin(-0.95);
        
		final XYLineAndShapeRenderer renderer4 = new XYLineAndShapeRenderer();
        renderer4.setSeriesLinesVisible(1, false);
        renderer4.setSeriesShapesVisible(0, false);
        plot4.setRenderer(renderer4);
	
		chartPanel1 = new ChartPanel(chartNLambda);
		chartPanel2 = new ChartPanel(chartDeltaLambda);
		chartPanel3 = new ChartPanel(chartDeltaPhi);
		chartPanel4 = new ChartPanel(chartDeltaN);

		bottomPanel.add(chartPanel1);
		bottomPanel.add(chartPanel2);
		bottomPanel.add(chartPanel3);
		bottomPanel.add(chartPanel4);
		
		add (bottomPanel, BorderLayout.PAGE_END);		
	}
			
	//Funkcja do odczytywania z pliku
	 static String Text(int n, File file) throws FileNotFoundException{
	    	String translation="";
	    	try{
	    		in = new Scanner(new FileInputStream(file), "UTF-8");
	    		for(int i=0; i<n; i++){
	    			in.nextLine();
	    		}
	    			
	    		translation=in.nextLine();
	       		return translation;
	    	}
	    	catch (FileNotFoundException e){
	    	    System.out.println("Plik sie nie otwiera");
	    	    return translation;
	    	}
		}
	 
	 
	 //MAIN
	public static void main(String[] args) throws HeadlessException, IOException {
		Window window = new Window();
		window.setVisible(true);
	}
	
}