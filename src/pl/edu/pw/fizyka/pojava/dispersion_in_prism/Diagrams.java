/* This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*/
/**Agnieszka Kołakowska**/
/**Dyspersja w pryzmacie**/
package pl.edu.pw.fizyka.pojava.dispersion_in_prism;


import javax.vecmath.Vector2f;

import org.jfree.data.xy.XYSeries;


public class Diagrams {
	
	private XYSeries nLambda;
	private XYSeries deltaLambda;
	private XYSeries deltaPhi;
	private XYSeries deltaN;
	
	private XYSeries pointNLambda;
	private XYSeries pointDeltaLambda;
	private XYSeries pointDeltaPhi;
	private XYSeries pointDeltaN;
	
	
	Calculator calc;
	
	public Diagrams(Calculator calc) {
		this.calc = calc;
		nLambda = new XYSeries("nLambda");
		deltaLambda = new XYSeries("deltaLambda");
		deltaPhi = new XYSeries("deltaPhi");
		deltaN = new XYSeries("deltaN");
		
		pointNLambda = new XYSeries("pointNLambda");
		pointDeltaLambda= new XYSeries("pointDeltaLambda");
		pointDeltaPhi= new XYSeries("pointDeltaPhi");
		pointDeltaN= new XYSeries("pointDeltaN");
	}
	
	public XYSeries getNLambda(){
		
		return nLambda;
	}
	
	public XYSeries getDeltaLambda(){
		
		return deltaLambda;
	}
	
	public XYSeries getDeltaPhi(){
		
		return deltaPhi;
	}
	
	public XYSeries getDeltaN(){
		
		return deltaN;
	}
	
	
	public XYSeries getPointNLambda() {
		return pointNLambda;
	}

	public XYSeries getPointDeltaLambda() {
		return pointDeltaLambda;
	}

	public XYSeries getPointDeltaPhi() {
		return pointDeltaPhi;
	}

	public XYSeries getPointDeltaN() {
		return pointDeltaN;
	}

	
	
	public void calcNLambda(String typeGlass, int lambda){
		for(int n=380; n<780; n++){
			nLambda.add(n, calc.calculateN(typeGlass, n));
		}
		
		
	}
	
	public void calcDeltaLambda(String typeGlass, int lambda, float apexAngle, int lightDimensionX, int lightDimensionY){
		for(int n=380; n<780; n++){
			if(calcDeta(typeGlass, n, apexAngle, lightDimensionX, lightDimensionY)!=null)
				deltaLambda.add(n, calcDeta(typeGlass, n, apexAngle, lightDimensionX, lightDimensionY));
		}
		
	}
	
	public void calcDeltaPhi(String typeGlass, int lambda, float apexAngle, int lightDimensionX, int lightDimensionY){
		for(int n=1; n<90; n++){
			if(calcDeta(typeGlass, lambda, n, lightDimensionX, lightDimensionY)!=null)
				deltaPhi.add(n, calcDeta(typeGlass, lambda, n, lightDimensionX, lightDimensionY));
		}
		
	}
	
	public void calcDeltaN(String typeGlass, int lambda, float apexAngle, int lightDimensionX, int lightDimensionY){
		for(int n=380; n<780; n++){
			if(calcDeta(typeGlass, lambda, apexAngle, lightDimensionX, lightDimensionY)!=null)
				deltaN.add(calc.calculateN(typeGlass, n), calcDeta(typeGlass, lambda, apexAngle, lightDimensionX, lightDimensionY));
		}
		
	}
		
	private void calcPointDeltaN(String typeGlass, int lambda, float apexAngle, int lightDimensionX, int lightDimensionY) {
		if(calcDeta(typeGlass, lambda, apexAngle, lightDimensionX, lightDimensionY)!=null)
			pointDeltaN.add(calc.calculateN(typeGlass, lambda), calcDeta(typeGlass, lambda, apexAngle, lightDimensionX, lightDimensionY));
			
		
		
	}

	private void calcPointDeltaPhi(String typeGlass, int lambda, float apexAngle, int lightDimensionX, int lightDimensionY) {
		if(calcDeta(typeGlass, lambda, apexAngle, lightDimensionX, lightDimensionY)!=null)
			pointDeltaPhi.add(apexAngle, calcDeta(typeGlass, lambda, apexAngle, lightDimensionX, lightDimensionY));
		
	}

	private void calcPointDeltaLambda(String typeGlass, int lambda, float apexAngle, int lightDimensionX, int lightDimensionY) {
		if(calcDeta(typeGlass, lambda, apexAngle, lightDimensionX, lightDimensionY)!=null)
			pointDeltaLambda.add(lambda, calcDeta(typeGlass, lambda, apexAngle, lightDimensionX, lightDimensionY));	
	}

	private void calcPointNLambda(String typeGlass, int lambda) {
		pointNLambda.add(lambda, calc.calculateN(typeGlass, lambda));
		
	}
	
	public void process(String typeGlass, int lambda, float apexAngle, int lightDimensionX, int lightDimensionY){
		this.nLambda.clear();
		this.pointNLambda.clear();
		this.deltaLambda.clear();
		this.pointDeltaLambda.clear();
		this.deltaPhi.clear();
		this.pointDeltaPhi.clear();
		this.deltaN.clear();
		this.pointDeltaN.clear();
		
		this.calcNLambda(typeGlass, lambda);
		this.calcDeltaLambda(typeGlass, lambda, apexAngle, lightDimensionX, lightDimensionY);
		this.calcDeltaPhi(typeGlass, lambda, apexAngle, lightDimensionX, lightDimensionY);
		this.calcDeltaN(typeGlass, lambda, apexAngle, lightDimensionX, lightDimensionY);
		
		this.calcPointNLambda(typeGlass, lambda);
		this.calcPointDeltaLambda(typeGlass, lambda, apexAngle, lightDimensionX, lightDimensionY);
		this.calcPointDeltaPhi(typeGlass, lambda, apexAngle, lightDimensionX, lightDimensionY);
		this.calcPointDeltaN(typeGlass, lambda, apexAngle, lightDimensionX, lightDimensionY);
		
	}
	
	public Float calcDeta(String typeGlass, int lambda, float angle, int lightDimensionX, int lightDimensionY){
		
		double n = calc.calculateN(typeGlass ,lambda);
		//TROJKAT
		VectorExtented2f vec1 = new VectorExtented2f(0.0f , -180.0f); 
		VectorExtented2f vec2= new VectorExtented2f(0.0f , 180.0f);
				
		VectorExtented2f vec9 = vec1.getRotatedWith(-(angle/2), vec2);
		VectorExtented2f vec7 = vec1.getRotatedWith(angle/2, vec2);
				
		float dif = vec9.getY() - vec1.getY();
				
		vec9.setY(vec9.getY()-dif);
		vec7.setY(vec7.getY()-dif);
				
		Line l8 = new Line(vec9, vec2);//scianka
		Line l5 = new Line(vec2, vec7); //druga scianka trojkata
		Line l9 = new Line(vec9, vec7);//podstawa trojkata
				
				
		//Promienie i rysowanie promieni
		//Promien padajacy
		VectorExtented2f vec3 = new VectorExtented2f(-70*lightDimensionY , lightDimensionX*lightDimensionY);
		VectorExtented2f vec4 = new VectorExtented2f(lightDimensionY , lightDimensionY);
		Line l2 = new Line(vec3, vec4); //linia promienia1
				
		VectorExtented2f vec5 = calc.intersectionPoint(l8, l2); //pierwszy punkt przeciecia
		//czy punkt przeciecia znajduje sie na rysowanej sciance
		if(vec5.getY()<vec2.getY() && vec5.getY()>vec9.getY()){			
		}
		else if(vec5.getY()>vec2.getY()){
			return null;
		}
		else{
			return null;
		}
		Line l3 = l8.getNormal(vec5); //normalna
		//Kat padania
		float alpha = calc.calculateIncidenceAngle(l2, l3); //alpha w stopniach
		Line l4 = calc.refractionFirst(alpha, l3, vec5, n); 
		VectorExtented2f vec8 = calc.intersectionPoint(l4, l5); //punkt przeciecia2
		Line ray;
		Line l6;
		//kat padania na 2 scianke
		float gamma;
		//czy punkt przeciecia znajduje sie na rysowanej sciance
		if(vec8.getY()<vec2.getY() && vec8.getY()>vec7.getY()){
			//visualisation.addLine(new Vertex(vec5, myColor), new Vertex(vec8, myColor)); //promien w srodku
			ray = l5;
			l6 = ray.getNormal(vec8); //normalna2
			//kat padania na 2 scianke
			Vector2f tmp1 = new VectorExtented2f(vec5.getX()-vec8.getX(), vec5.getY()-vec8.getY());
			VectorExtented2f tmp2 = new VectorExtented2f(l6.vectorM1000().getX()-vec8.getX(), l6.vectorM1000().getY()-vec8.getY());
			gamma = (float) (tmp1.angle(tmp2)*180/Math.PI);				
		}
		else{
			ray = l9;
			vec8 = calc.intersectionPoint(l4, ray); //punkt przeciecia2
			//VectorExtented2f normal = l9.getVectorNormal();
			l6 = new Line(vec8, new VectorExtented2f(vec8.getX(), 1000));
			Line l10 = new Line(vec5, vec8);
			gamma = (float) (Math.atan(l10.getA())*180/Math.PI); //stopnie
		}
		//sprawdzanie calkowitego wewnetrznego odbicia
		//Line l7;
		//boolean lineSeven = true;
		//VectorExtented2f odbite = null;
		if(Math.abs(gamma)<(Math.asin(1/n)*180/Math.PI)){
		//promien wychodzacy
			return new Float((float) (refractionSecond(gamma, l6, vec8, n)*180/Math.PI));
		}
		else{
			return null;
		}
		
		
	}
	
	public Line refractionFirst(float alpha, Line normal, VectorExtented2f intersectionPoint, double n){
		float beta = (float)((Math.asin(Math.sin(Math.abs(alpha)*Math.PI/180)/n)));//Beta w radianach
		VectorExtented2f v= normal.getDircetion().getRotated(beta*180/(float)Math.PI);
		return new Line(v.getTranslated(intersectionPoint.getX(), intersectionPoint.getY()), intersectionPoint);
	}
	
	public float refractionSecond(float alpha, Line normal, VectorExtented2f intersectionPoint, double n){
		return (float)((Math.asin(Math.sin(Math.abs(alpha)*Math.PI/180)*n)));//delta w radianach
		
	}
}
