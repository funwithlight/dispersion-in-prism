/* This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*/
/**Agnieszka Kołakowska**/
/**Dyspersja w pryzmacie**/
package pl.edu.pw.fizyka.pojava.dispersion_in_prism;
import java.util.ArrayList;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;

class Visualisation implements GLEventListener 
{
    private GLU glu = new GLU();
     
    //Listy wierzcholkow/odcinkow do rysowania
    ArrayList<Vertex> vertices = new ArrayList<Vertex>();
    ArrayList<Vertex> linesVertices = new ArrayList<Vertex>();
    
    
    public void addVertex(Vertex v){
    	vertices.add(v);
    }
    
    public void addLine(Vertex v1, Vertex v2) {
    	linesVertices.add(v1);
    	linesVertices.add(v2);
    }
 
    public void display(GLAutoDrawable gLDrawable){
        final GL2 gl = gLDrawable.getGL().getGL2();
        //Czyszczenie bufora koloru i glebi
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
        gl.glLoadIdentity();
        gl.glLineWidth(2); //grubosc lini
        gl.glPointSize(10);
             
        gl.glBegin(GL2.GL_LINES);	
        for (int i = 0; i<linesVertices.size(); i++){
        	linesVertices.get(i).paintColor(gl);
        	gl.glVertex2f(linesVertices.get(i).getX(), linesVertices.get(i).getY());
        }
		gl.glEnd();
		
		gl.glBegin(GL2.GL_POINTS);	
        for (int i = 0; i<vertices.size(); i++){
        	vertices.get(i).paintColor(gl);
        	gl.glVertex2f(vertices.get(i).getX(), vertices.get(i).getY());
        }
		gl.glEnd(); 
    }
 
    public void displayChanged(GLAutoDrawable gLDrawable, boolean modeChanged, boolean deviceChanged) {    
    }
    
    //Inicjalizacja
    public void init(GLAutoDrawable gLDrawable){
        GL2 gl = gLDrawable.getGL().getGL2();
        gl.glClearColor( 0.0f, 0.0f, 0.0f, 1.0f);
    }
    //Zmiana rozmiaru okienka,  uklad wspolrzednych
    public void reshape(GLAutoDrawable gLDrawable, int x, int y, int width, int height) 
    {
        final GL2 gl = gLDrawable.getGL().getGL2();
 
        if (height <= 0){ // dzielenie przez 0
            height = 1;
        }
        
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluOrtho2D ( -(width/2), (width/2), -(height/2), (height/2) );
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
    }
 
    //Zakończenie
	public void dispose(GLAutoDrawable arg0){
		
	}
	
	public void clear(){
		vertices.clear();
		linesVertices.clear();
	}
}