/* This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*/
/**Agnieszka Ko�akowska**/
/**Dyspersja w pryzmacie**/
package pl.edu.pw.fizyka.pojava.dispersion_in_prism;

import javax.vecmath.Tuple2d;
import javax.vecmath.Tuple2f;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector2f;

public class VectorExtented2f extends Vector2f {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VectorExtented2f() {
		// TODO Auto-generated constructor stub
	}
	
	public VectorExtented2f(VectorExtented2f arg0) {
		this.setX(arg0.getX());
		this.setY(arg0.getY());
	}
	
	public VectorExtented2f(float[] arg0) {
		super(arg0);
		
		// TODO Auto-generated constructor stub
	}

	public VectorExtented2f(Vector2f arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public VectorExtented2f(Vector2d arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public VectorExtented2f(Tuple2f arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public VectorExtented2f(Tuple2d arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public VectorExtented2f(float arg0, float arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}
	
	//translacja
	public void translate(float tX, float tY){
		float x = this.getX();
		float y = this.getY();
		this.setX(x+tX);
		this.setY(y+tY);	
	}
	
	public VectorExtented2f getTranslated(float tX, float tY){
		return new VectorExtented2f((this.getX()+tX), (this.getY()+tY));	
	}
	
	//rotacja
	public void rotate(float alpha){ //alpha w STOPNIACH!!!
		float x = this.getX();
		float y = this.getY();
		this.setX((float) (x*Math.cos(alpha*Math.PI/180)-(float)y*Math.sin(alpha*Math.PI/180)));
		this.setY((float) (x*Math.sin(alpha*Math.PI/180)+(float)y*Math.cos(alpha*Math.PI/180)));
		
	}
	
	public VectorExtented2f getRotated(float alpha){
		//float x = this.getX();
		return new VectorExtented2f(((float)(getX()*Math.cos(alpha*Math.PI/180)-(float)this.getY()*Math.sin(alpha*Math.PI/180))), ((float)(getX()*Math.sin(alpha*Math.PI/180)+(float)this.getY()*Math.cos(alpha*Math.PI/180))));
	}
	
	public VectorExtented2f getRotatedWith(float alpha, VectorExtented2f v){
		float x = this.getX()-v.getX();
		float y = this.getY()-v.getY();
		return new VectorExtented2f(((float)(x*Math.cos(alpha*Math.PI/180)-(float)y*Math.sin(alpha*Math.PI/180))+v.getX()), ((float)(x*Math.sin(alpha*Math.PI/180)+(float)y*Math.cos(alpha*Math.PI/180)))+v.getY());
	}

	
	//skalowanie
	public void scale(float sX, float sY){
		float x = this.getX();
		float y = this.getY();
		this.setX(x*sX);
		this.setY(y*sY);	
		
	}
	
	public VectorExtented2f getScaled(float sX, float sY){
		return new VectorExtented2f((this.getX()*sX), (this.getY()*sY));
	}
	
	//K�t pomi�dzy dwoma wektorami
	public float getAngleBetween(VectorExtented2f v1){
		return this.angle(v1)*180/(float)Math.PI;	//Wypluwa stopnie
	}
	

}
