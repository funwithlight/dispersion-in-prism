Pryzmat
=======
Zjawisko dyspersji w pryzmacie

Autor: Agnieszka Ko�akowska
Cel: praca zaliczeniowa z przedmiotu Programowanie Obiektowe WF PW 2014
Wykorzystane technologie: Java, JOGL, VecMath, JFreeChart
https://bitbucket.org/funwithlight/dispersion-in-prism/
Ostateczna wersja: https://bitbucket.org/funwithlight/dispersion-in-prism/downloads

Przyk�adowe u�ycie: U�ytkownik ma mo�liwo�� zmieni� parametry pryzmatu i �r�d�a �wiat�a, nast�pnie 
mo�e zobaczy� bieg geometryczny promieni w pryzmacie. Za ka�dym razem, kiedy zmienia 
parametry, zmienia si� jego po�o�enie wy�wietlanych na sta�e na wykresach. W ten 
spos�b mo�e zaobserwowa� w jaki spos�b zmienia si� k�t za�amania promienia 
wychodz�cego dla r�nych parametr�w.


